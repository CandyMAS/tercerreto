#Author: your.email@your.domain.com
#language:es

@Regresion
Característica: Gestionar Cita Medica
  	Como paciente
		Quiero realizar la solicitud de una cita medica
		A traves del sistema de Administracion de Hospitales

  @RealizarRegistroDoctor
Esquema del escenario: Realizar el Registro de un Doctor  
    Dado que Carlos necesita registrar un nuevo doctor
    Cuando el realiza el registro del mismo en el aplicativo de Administracion de Hospitales
    |NombreCompleto|Apellidos|Telefono|TipoDocumento|Documentoidentidad|
    |<NombreCompleto>|<Apellidos>|<Telefono>|<TipoDocumento>|<Documentoidentidad>|
    Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente.
    
Ejemplos:

 |NombreCompleto|Apellidos|Telefono|TipoDocumento|Documentoidentidad|
 |Juan Fernando|Jaramillo|4442255|Pasaportes|1128456723|
 
  @RealizarRegistroPaciente
Esquema del escenario: Realizar el Registro de un Paciente  
    Dado que Carlos necesita registrar un nuevo paciente
    Cuando el realiza el registro del mismo en el aplicativo de Administración de Hospitales
    |NombreCompleto|Apellidos|Telefono|TipoDocumento|Documentoidentidad|
    |<NombreCompleto>|<Apellidos>|<Telefono>|<TipoDocumento>|<Documentoidentidad>|
    Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente.
    
Ejemplos:

 |NombreCompleto|Apellidos|Telefono|TipoDocumento|Documentoidentidad|
 |Jhon |Perez|4442255|Pasaportes|1128456733|
 
  @RealizarAgendamientoDeUnaCita
Esquema del escenario: Realizar el Agendamiento de una Cita
Dado que Carlos necesita asistir al medico
Cuando el realiza el agendamiento de una Cita
 |DiaCita|DocumentoPaciente|DocumentoDoctor|Observaciones|
 |<DiaCita>|<DocumentoPaciente>|<DocumentoDoctor>|<Observaciones>|
Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente.

Ejemplos:

 |DiaCita|DocumentoPaciente|DocumentoDoctor|Observaciones|
 |09/19/2018|1128456732|1128456722|Cita Medica|
