package co.com.admonhospitales.screenplay.stepdefinitions;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.admonhospitales.screenplay.model.Cita;
import co.com.admonhospitales.screenplay.model.Doctor;
import co.com.admonhospitales.screenplay.model.Paciente;
import co.com.admonhospitales.screenplay.questions.MensajeEnPantalla;
import co.com.admonhospitales.screenplay.tasks.Abrir;
import co.com.admonhospitales.screenplay.tasks.Agendar;
import co.com.admonhospitales.screenplay.tasks.Crear;
import co.com.admonhospitales.screenplay.tasks.Registrar;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;


public class SistemaDeAdministracionDeHospitalesStepDefinitions {
	
	@Managed(driver="chrome")
	private WebDriver SuNavegador;
	private Actor Carlos = Actor.named("Carlos");
	
	@Before
	public void configuracionInicial()
	{
		Carlos.can(BrowseTheWeb.with(SuNavegador));
	}
	
	
	@Dado("^que Carlos necesita registrar un nuevo doctor$")
	public void queCarlosNecesitaRegistrarUnNuevoDoctor() throws Exception {
		Carlos.wasAbleTo(Abrir.LaPaginaDelSistemaDeAdministraciónDeHospitales());
	    
	}
	
	@Cuando("^el realiza el registro del mismo en el aplicativo de Administracion de Hospitales$")
	public void elRealizaElRegistroDelMismoEnElAplicativoDeAdministracionDeHospitales(List<Doctor> doctor) throws Exception {
		Carlos.attemptsTo(Registrar.UnNuevo(doctor));
	}

	
	@Entonces("^el verifica que se presente en pantalla el mensaje (.*)$")
	public void elVerificaQueSePresenteEnPantallaElMensajeDatosGuardadosCorrectamente(String MensajeEsperado) throws Exception {
		Carlos.should(GivenWhenThen.seeThat(MensajeEnPantalla.es(), Matchers.equalTo(MensajeEsperado)));
	 
	}
	
	@Dado("^que Carlos necesita registrar un nuevo paciente$")
	public void queCarlosNecesitaRegistrarUnNuevoPaciente() throws Exception {
		Carlos.wasAbleTo(Abrir.LaPaginaDelSistemaDeAdministraciónDeHospitales());
	}


	@Cuando("^el realiza el registro del mismo en el aplicativo de Administración de Hospitales$")
	public void elRealizaElRegistroDelMismoEnElAplicativoDeAdministraciónDeHospitales(List<Paciente> paciente) throws Exception {
		Carlos.attemptsTo(Crear.UnNuevo(paciente));			  
	}

	@Dado("^que Carlos necesita asistir al medico$")
	public void queCarlosNecesitaAsistirAlMedico() throws Exception {
		Carlos.wasAbleTo(Abrir.LaPaginaDelSistemaDeAdministraciónDeHospitales());
	}


	@Cuando("^el realiza el agendamiento de una Cita$")
	public void elRealizaElAgendamientoDeUnaCita(List<Cita> cita) throws Exception {
		Carlos.attemptsTo(Agendar.UnaNueva(cita));	
	}
	
}


