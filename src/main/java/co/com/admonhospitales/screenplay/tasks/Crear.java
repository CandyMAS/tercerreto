package co.com.admonhospitales.screenplay.tasks;

import java.util.List;

import co.com.admonhospitales.screenplay.model.Paciente;
import co.com.admonhospitales.screenplay.ui.SistemaDeAdministracionDeHospitalesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Crear implements Task {
	
private List<Paciente> Datos;
	
	public Crear(List<Paciente> Datos)
	{
		this.Datos=Datos;
	}
	

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(SistemaDeAdministracionDeHospitalesPage.AGREGAR_PACIENTE));
		actor.attemptsTo(Enter.theValue(Datos.get(0).getNombreCompleto()).into(SistemaDeAdministracionDeHospitalesPage.NOMBRE_COMPLETO_PACIENTE));
		actor.attemptsTo(Enter.theValue(Datos.get(0).getApellidos()).into(SistemaDeAdministracionDeHospitalesPage.APELLIDOS_PACIENTE));
		actor.attemptsTo(Enter.theValue(Datos.get(0).getTelefono()).into(SistemaDeAdministracionDeHospitalesPage.TELEFONO_PACIENTE));
		actor.attemptsTo(SelectFromOptions.byVisibleText(Datos.get(0).getTipoDocumento()).from(SistemaDeAdministracionDeHospitalesPage.TIPO_DOCUMENTO_PACIENTE));
		actor.attemptsTo(Enter.theValue(Datos.get(0).getDocumentoidentidad()).into(SistemaDeAdministracionDeHospitalesPage.DOCUMENTO_PACIENTE));
		actor.attemptsTo(Click.on(SistemaDeAdministracionDeHospitalesPage.BOTON_SALUD_PREPAGADA));
		actor.attemptsTo(Click.on(SistemaDeAdministracionDeHospitalesPage.BOTON_GUARDAR_PACIENTE));
		
	}

	public static Crear UnNuevo(List<Paciente> Datos) {
		
		return Tasks.instrumented(Crear.class,Datos);
	}

}
