package co.com.admonhospitales.screenplay.tasks;

import java.util.List;

import co.com.admonhospitales.screenplay.model.Cita;
import co.com.admonhospitales.screenplay.model.Paciente;
import co.com.admonhospitales.screenplay.ui.SistemaDeAdministracionDeHospitalesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Agendar implements Task {

private List<Cita> Datos;
	
	public Agendar(List<Cita> Datos)
	{
		this.Datos=Datos;
	}
	

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(SistemaDeAdministracionDeHospitalesPage.AGREGAR_CITA));
		actor.attemptsTo(Enter.theValue(Datos.get(0).getdiaCita()).into(SistemaDeAdministracionDeHospitalesPage.DIA_CITA));
		actor.attemptsTo(Enter.theValue(Datos.get(0).getdocumentoPaciente()).into(SistemaDeAdministracionDeHospitalesPage.DOCUMENTO_PACIENTE_CITA));
		actor.attemptsTo(Enter.theValue(Datos.get(0).getdocumentoDoctor()).into(SistemaDeAdministracionDeHospitalesPage.DOCUMENTO_DOCTOR_CITA));
		actor.attemptsTo(Enter.theValue(Datos.get(0).getobservaciones()).into(SistemaDeAdministracionDeHospitalesPage.OBSERVACIONES_CITA));			
		actor.attemptsTo(Click.on(SistemaDeAdministracionDeHospitalesPage.BOTON_GUARDAR_CITA));
		
	}
	
public static Agendar UnaNueva(List<Cita> Datos) {
		
	return Tasks.instrumented(Agendar.class,Datos);
	}

}
