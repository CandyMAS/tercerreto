package co.com.admonhospitales.screenplay.tasks;

import co.com.admonhospitales.screenplay.tasks.Abrir;
import co.com.admonhospitales.screenplay.ui.SistemaDeAdministracionDeHospitalesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task {
	
	private SistemaDeAdministracionDeHospitalesPage sistemaDeAdministracionDeHospitalesPage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(sistemaDeAdministracionDeHospitalesPage));
		
	}

	public static Abrir LaPaginaDelSistemaDeAdministraciónDeHospitales() {
		
		return Tasks.instrumented(Abrir.class);
	}

}
