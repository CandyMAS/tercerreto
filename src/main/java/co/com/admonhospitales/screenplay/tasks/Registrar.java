package co.com.admonhospitales.screenplay.tasks;

import java.util.List;

import co.com.admonhospitales.screenplay.model.Doctor;
import co.com.admonhospitales.screenplay.tasks.Registrar;
import co.com.admonhospitales.screenplay.ui.SistemaDeAdministracionDeHospitalesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Registrar implements Task {
    
private List<Doctor> Datos;
	
	public Registrar(List<Doctor> Datos)
	{
		this.Datos=Datos;
	}
	
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(SistemaDeAdministracionDeHospitalesPage.AGREGAR_DOCTOR));
		actor.attemptsTo(Enter.theValue(Datos.get(0).getNombreCompleto()).into(SistemaDeAdministracionDeHospitalesPage.NOMBRE_COMPLETO));
		actor.attemptsTo(Enter.theValue(Datos.get(0).getApellidos()).into(SistemaDeAdministracionDeHospitalesPage.APELLIDOS));
		actor.attemptsTo(Enter.theValue(Datos.get(0).getTelefono()).into(SistemaDeAdministracionDeHospitalesPage.TELEFONO));
		actor.attemptsTo(SelectFromOptions.byVisibleText(Datos.get(0).getTipoDocumento()).from(SistemaDeAdministracionDeHospitalesPage.TIPO_DOCUMENTO));
		actor.attemptsTo(Enter.theValue(Datos.get(0).getDocumentoidentidad()).into(SistemaDeAdministracionDeHospitalesPage.DOCUMENTO));
		actor.attemptsTo(Click.on(SistemaDeAdministracionDeHospitalesPage.BOTON_GUARDAR));

		//actor.attemptsTo(Enter.theValue(Datos.get(O).getNombre()).into(RegistroInformacionPage.DOCTOR));	
		
				
	}

	public static Registrar UnNuevo(List<Doctor> Datos) {
		// TODO Auto-generated method stub
		return Tasks.instrumented(Registrar.class,Datos);
	}
}



	
