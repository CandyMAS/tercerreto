package co.com.admonhospitales.screenplay.questions;

import co.com.admonhospitales.screenplay.questions.MensajeEnPantalla;
import co.com.admonhospitales.screenplay.ui.SistemaDeAdministracionDeHospitalesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class MensajeEnPantalla implements Question<String> {

	public static MensajeEnPantalla es() {
		return new MensajeEnPantalla();
	}

	@Override
	public String answeredBy(Actor actor) {
		
		return Text.of(SistemaDeAdministracionDeHospitalesPage.MENSAJE).viewedBy(actor).asString();
	}
}

