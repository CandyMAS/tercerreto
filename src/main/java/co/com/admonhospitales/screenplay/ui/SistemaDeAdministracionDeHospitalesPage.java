package co.com.admonhospitales.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")
public class SistemaDeAdministracionDeHospitalesPage extends PageObject {
	
	public static final Target AGREGAR_DOCTOR = Target.the("Campo para seleccionar la opción de ingresar Doctor")
			.located(By.xpath("//a[@href='addDoctor']" ));
	public static final Target NOMBRE_COMPLETO = Target.the("Campo para ingresar el nombre completo del doctor")
			.located(By.id("name" ));
	public static final Target APELLIDOS = Target.the("Campo para ingresar los apellidos del doctor")
			.located(By.id("last_name" ));
	public static final Target TELEFONO = Target.the("Campo para ingresar el telefono del doctor")
			.located(By.id("telephone" ));
	public static final Target TIPO_DOCUMENTO = Target.the("Campo para seleccionar el tipo de documento del doctor")
			.located(By.xpath(("//select[@id='identification_type']")));	
	public static final Target DOCUMENTO = Target.the("Campo para ingresar el documento del doctor")
			.located(By.id("identification" ));
	public static final Target BOTON_GUARDAR = Target.the("Botón para guardar la información del doctor")
			.located(By.xpath("//*[@id='page-wrapper']/div/div[3]/div/a" ));
	public static final Target MENSAJE = Target.the("Label que presenta la mensaje de confirmación")
			.located(By.className("panel-body" ));
	
	public static final Target AGREGAR_PACIENTE = Target.the("Campo para seleccionar la opción de ingresar Paciente")
			.located(By.xpath("//a[@href='addPatient']" ));
	public static final Target NOMBRE_COMPLETO_PACIENTE = Target.the("Campo para ingresar el nombre completo del paciente")
			.located(By.xpath("//input[@name='name']" ));
	public static final Target APELLIDOS_PACIENTE = Target.the("Campo para ingresar los apellidos del paciente")
			.located(By.xpath("//input[@name='last_name']" ));
	public static final Target TELEFONO_PACIENTE = Target.the("Campo para ingresar el telefono del paciente")
			.located(By.xpath("//input[@name='telephone']" ));
	public static final Target TIPO_DOCUMENTO_PACIENTE = Target.the("Campo para seleccionar el tipo de documento del pacientee")
			.located(By.xpath(("//select[@name='identification_type']")));	
	public static final Target DOCUMENTO_PACIENTE = Target.the("Campo para ingresar el documento del paciente")
			.located(By.xpath(("//input[@name='identification']")));
	public static final Target BOTON_SALUD_PREPAGADA = Target.the("Campo para seleccionar si el paciente tiene o no salud prepagada")
			.located(By.xpath(("//div[@class='checkbox']//input[@name='prepaid']")));
	public static final Target BOTON_GUARDAR_PACIENTE = Target.the("Botón para guardar la información del paciente")
			.located(By.xpath(("//*[@class='btn btn-primary pull-right']")));
	
	
	public static final Target AGREGAR_CITA = Target.the("Campo para seleccionar la opción para agendar una cita")
			.located(By.xpath("//a[@href='appointmentScheduling']" ));
	public static final Target DIA_CITA = Target.the("Campo para seleccionar el día de la cita")
			.located(By.xpath("//div[@class='form-group']//input[@id='datepicker']" ));	
	public static final Target DOCUMENTO_PACIENTE_CITA = Target.the("Campo para ingresar el documento del paciente en la cita")
			.located(By.xpath("//div[@class='form-group']//input[@class='form-control']" ));
	public static final Target DOCUMENTO_DOCTOR_CITA = Target.the("Campo para ingresar el documento del doctor cita")
			.located(By.xpath("//div[@class='form-group']//input[@placeholder='Ingrese el documento de identidad del doctor']" ));
	public static final Target OBSERVACIONES_CITA = Target.the("Campo para ingresar las observaciones de la cita")
			.located(By.xpath("//textarea[@rows='3']" ));
	public static final Target BOTON_GUARDAR_CITA = Target.the("Botón para guardar la información de la cita")
			.located(By.xpath(("//*[@class='btn btn-primary pull-right']")));
		
	
	
}
