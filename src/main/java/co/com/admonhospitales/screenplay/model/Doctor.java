package co.com.admonhospitales.screenplay.model;

public class Doctor {
	
	private String nombreCompleto;
	private String apellidos;
	private String telefono;
	private String tipoDocumento;
	private String documentoidentidad;
	
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	
	public String getApellidos() {
		return apellidos;
	}
	
	public String getTelefono() {
		return telefono;
	}
	
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	
	public String getDocumentoidentidad() {
		return documentoidentidad;
	}
	

}
