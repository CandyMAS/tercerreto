package co.com.admonhospitales.screenplay.model;

import java.sql.Date;

public class Cita {
	
	private String diaCita;
	private String documentoPaciente;
	private String documentoDoctor;
	private String observaciones;
	
	public String getdiaCita() {
		return diaCita;
	}
	
	public String getdocumentoPaciente() {
		return documentoPaciente;
	}
	
	public String getdocumentoDoctor() {
		return documentoDoctor;
	}
	
	public String getobservaciones() {
		return observaciones;
	}
	
	
}
