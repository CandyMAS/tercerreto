#Author: your.email@your.domain.com
#language:es

@Regresion
CaracterÝstica: Gestionar Cita Medica
  	Como paciente
		Quiero realizar la solicitud de una cita medica
		A traves del sistema de Administracion de Hospitales

  @RealizarRegistroDoctor
Esquema del escenario: Realizar el Registro de un Doctor  
    Dado que Carlos necesita registrar un nuevo doctor
    Cuando el realiza el registro del mismo en el aplicativo de Administracion de Hospitales
    |NombreCompleto|Apellidos|Telefono|TipoDocumento|Documentoidentidad|
    |<NombreCompleto>|<Apellidos>|<Telefono>|<TipoDocumento>|<Documentoidentidad>|
    Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente.
    
Ejemplos:

 |NombreCompleto|Apellidos|Telefono|TipoDocumento|Documentoidentidad|
 |Juan Fernando|Jaramillo|4442255|Pasaportes|1128456786|